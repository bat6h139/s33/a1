import mongoose from 'mongoose';

// Schema
const courseSchema = new mongoose.Schema({
    courseName: {
        type: String,
        required: [true, "Course name is required"]
    },
    courseDesc: {
        type: String,
        required: [true, "Course description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    enrollees: [
        {
            userId: {
                type: String,
                required: [true, "User ID is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
}, { timestamps: true });

// Model
export default mongoose.model('Course', courseSchema);